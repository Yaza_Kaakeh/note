<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Note;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UsersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $temp = [
            'name' => 'App Tech',
            'email' => 'asd@asd.com',
            'password' => bcrypt('123123123'),

        ];
        \App\Models\User::create($temp);
        $faker = Factory::create();
        for ($i = 0; $i < 2; $i++) {
            $notes = new Note();
            $notes->user_id = 1;
            $notes->note_title = $faker->words(2,true);
            $notes->description = $faker->paragraph(2);
            $notes->slug = $faker->slug(2,true);
            $notes->save();

        }
        for ($s = 0; $s < 5; $s++) {
            $customer = new Customer();
            $customer->name = $faker->words(2,true);
            $customer->subscription_end = $faker->date($format = 'Y-m-d');
            $customer->save();

        }
        for ($a = 0; $a < 5; $a++) {
            $customer = new Customer();
            $customer->name = $faker->words(2,true);
            $customer->subscription_end = $faker->date($format = 'Y-m-d');
            $customer->save();

        }
    }
}
