<body id="body-pd">
<header class="header" id="header">
    <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
</header>
<div class="l-navbar" id="nav-bar">
    <nav class="nav">
        <div>
            <a href="{{route("frontend")}}" class="nav_logo">
                <i class='bx bx-layer nav_logo-icon'>

                </i>
                <span class="nav_logo-name">
                   {{__('model.side_bar.apptech')}}
                </span>
            </a>
            <div class="nav_list">
                <a href="{{route("home")}}" class="nav_link active">
                    <i class='bx bx-grid-alt nav_icon'>
                    </i>
                    <span class="nav_name">
                        {{__('model.side_bar.dashboard')}}
                    </span>
                </a>
              {{--  <a href="#" class="nav_link">
                    <i class='bx bx-user nav_icon'>

                    </i>
                    <span class="nav_name">
                        Users
                    </span>
                </a>
                <a href="#"
                   class="nav_link">
                    <i class='bx bx-message-square-detail nav_icon'>

                    </i> <span class="nav_name">
                        Messages
                    </span>
                </a>--}}
                <a href="{{route('notes.index')}}" class="nav_link">
                    <i class='bx bx-bookmark nav_icon'>

                    </i> <span class="nav_name">
                       {{__('model.side_bar.notes')}}
                    </span>
                </a>
                <a href="#" class="nav_link">
                    <i class='bx bx-folder nav_icon'>

                    </i> <span class="nav_name">
                        Files
                    </span>
                </a>
                <a href="#" class="nav_link">
                    <i class='bx bx-bar-chart-alt-2 nav_icon'>

                    </i>
                    <span class="nav_name">
                        Stats
                    </span>

                </a>
            </div>
        </div>
        <a href="{{ route('logout') }}" class="nav_link"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class='bx bx-log-out nav_icon'>

            </i>
            <span class="nav_name">
                {{ __('Logout') }}
            </span>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </a>
    </nav>
</div>
