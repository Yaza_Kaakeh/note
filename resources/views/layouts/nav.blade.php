<div class="bg-gray-50 h-16 w-full">
    <nav class="flex items-center justify-between px-64 mx-auto py-2">
        <img class="h-12" src="{{asset('images/src/logo.png')}}" alt="">
        <div>
            <ul>
                <li>
                    <a href="" class="nav-a" >
                        Home
                    </a>
                    <a href="" class="nav-a">
                        Services
                    </a>
                    <a href="" class="nav-a">
                        Clients
                    </a>
                    <a href="" class="nav-a">
                        News
                    </a>
                    <a href="" class="nav-a">
                        Contact
                    </a>
                </li>
            </ul>
        </div>
        <div>
            <button class="btn-nav">
                Sign Up
            </button>
            <button class="btn-nav">
                Log in
            </button>
        </div>

    </nav>
</div>