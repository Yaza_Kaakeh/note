@extends('layouts.app')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("home")}}">{{__('model.breadcrumb.home')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('notes.index')}}">{{__('modules/note.notesAtt.name')}}</a></li>
        </ol>
    </nav>
    <section class="pt-7 pt-md-1">
    <div class="container bg-light">
        <div class="form-group">
            <a href="{{route('notes.edit',$item->id)}}" class="btn btn-warning">@lang('modules/note.notesAtt.edit.title')</a>
            <a href="{{route('note.destroy',$item->id)}}" class="btn btn-danger">@lang('modules/note.notesAtt.delete.title')</a>
            <a href="{{route('show',$item->slug)}}" class="btn btn-primary">@lang('modules/note.notesAtt.show.title')</a>

        </div>



            <div class="row">
                <div class="col-12">
                    <div class="card" style="width: 18rem;">
                        <img src="{{asset('images/notes/'.$item->image)}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{$item->note_title}}</h5>
                            <p class="card-text">{{$item->description}}</p>
                            <a href="{{route('notes.show',$item->slug)}}" class="btn btn-primary">Show</a>
                        </div>
                    </div>
                </div>
            </div>


    </div>

    </section>


@endsection
