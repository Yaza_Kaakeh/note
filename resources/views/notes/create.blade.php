@extends('layouts.app')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("home")}}">{{__('model.breadcrumb.home')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('notes.index')}}">{{__('modules/note.notesAtt.name')}}</a></li>
        </ol>
    </nav>

    <div class="height-100 bg-light">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card-body">
                    @if(session()->has('error'))
                        <div class="alert alert-danger" style="padding: 20px;">
                            <p>
                                {{session()->get('error')}}
                            </p>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger" style="padding: 20px;">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                <form  method="POST" action="{{ route('notes.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="note_title">
                            @lang('modules/note.attr.note_title')</label>
                        <input id="note_title" type="text" class="form-control" name="note_title"
                               value="{{ old('note_title') }}"
                               data-v-p="string[required|range:4,190]" autofocus>
                        <div class="invalid-feedback"></div>
                    </div>

{{--
                        <div class="form-group">
                            <label for="slug">Link</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">{{env('APP_URL') . '/notes/'}}</span>
                                </div>
                                <input type="text" class="form-control"
                                       id="slug" name="slug" value="{{ old('slug') }}" readonly>
                            </div>
                        </div>
--}}

                    <div class="form-group">
                        <label for="description">
                            @lang('modules/note.attr.description')</label>
                        <input id="description" type="text" class="form-control" name="description"
                               value="{{ old('description') }}"
                               data-v-p="string[required|range:4,190]" autofocus>
                        <div class="invalid-feedback"></div>
                    </div>

                    <div class="form-group">
                        <label for="image">
                            @lang('modules/note.attr.image')</label>
                        <input id="image" type="file" class="form-control" name="image"
                               value="{{ old('image') }}"
                               data-v-p="string[required|range:4,190]" autofocus>
                        <div class="invalid-feedback"></div>
                    </div>

                    <button class="btn btn-success" id="spa-submit">
                        @lang('general.btn.save')
                    </button>
                </form>
            </div>
        </div>

    </div>

@endsection
