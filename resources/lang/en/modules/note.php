<?php
/**
 * Created by PhpStorm.
 * User: Usama
 * Date: 12/11/2017
 * Time: 1:46 AM
 */

return [

    'name' => 'Note',
    'attr' => [
        'note_title' => 'Note Title',
        'image' => 'Image',
        'slug' => 'Slug',
        'description' => 'Description',
    ],


    'notesAtt' => [
        'name' => 'Note',
        'name_short' => 'Note',
        'namePlural' => 'Notes',
        'create' => [
            'title' => 'Add Note',
        ],
        'show' => [
            'title' => 'Show Note',
        ],
        'edit' => [
            'title' => 'Edit Note',
        ],
        'delete' => [
            'title' => 'Delete Note',
        ],
    ],

];