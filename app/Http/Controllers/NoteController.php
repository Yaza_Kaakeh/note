<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteRequest;
use App\Layers\BusinessLayer\notes\NoteLayer;
use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NoteController extends Controller
{
    protected $accessLayer;
    public function __construct(NoteLayer $accessLayer)
    {
        $this->accessLayer = $accessLayer;
    }

    public function joinTest()
    {
        if (Auth::user()) {
            //$model = Note::get();
            $model = DB::table('notes')
                ->join('users','users.id',"=",'notes.user_id')
                ->get();
        }
        dd( $model);
        //return view('notes.index', compact('model'));

    }

    public function index()
    {
        if (Auth::user()) {
            $model = Note::get();
        }
        return view('notes.index', compact('model'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('notes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NoteRequest $request)
    {
        //$this->validate($request, $this->accessLayer->getValidationRules());

        $model = $this->accessLayer->store(
            $request->input('note_title'),
            $request->input('description'),
            $request
        );
        return redirect()->route('notes.show', $model->slug);

    }


    public function show($slug)
    {
        if (Auth::user()) {
            $item = Note::where('slug', $slug)->firstOrFail();
            return view('notes.show', compact('item'));
        } else
            redirect()->back();

    }

    public function showBuSlug($slug)
    {
        $item = Note::where('slug', $slug)->firstOrFail();
        return view('notes.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Note $note
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Note::find($id);
        return view('notes.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Note $note
     * @return \Illuminate\Http\Response
     */
    public function update(NoteRequest $request, $id)
    {
        $models = Note::findOrFail($id);
        $this->validate($request, $this->accessLayer->getValidationRules());

        $this->accessLayer->update(
            $models,
            $request->input('note_title'),
            $request->input('description'),
            $request
        );
        return redirect()->route('notes.show', $models->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Note $note
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $model = Note::find($id);
        $model->delete();

        return redirect()->route('notes.index');
    }
}
