<?php

namespace App\Http\Controllers\API\Note;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoteRequest;
use App\Http\Resources\NoteOneResource;
use App\Http\Resources\NoteResource;
use App\Layers\BusinessLayer\notes\NoteLayer;
use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoteApiController extends Controller
{
    private $accessLayer;
    public function __construct(NoteLayer $accessLayer)
    {
        $this->accessLayer = $accessLayer;
    }

    public function index()
    {
        if (Auth::user()){
            $model = Note::get();
        }
        if ($model)
            return response([
                'status' => 'success',
                'data' => NoteResource::collection($model),
            ], 200);
        return response([
            'status' => 'success',
            'data' => null,
        ], 200);

    }

    public function store(NoteRequest $request)
    {
        $model = $this->accessLayer->store(
            $request->input('note_title'),
            $request->input('description'),
            $request
        );
        return response([
            'status' => 'success',
            'data'=>  new NoteOneResource($model),
        ]);
    }

    public function show($slug)
    {
        $item = Note::where('slug',$slug)->firstOrFail();
        return response([
            'status' => 'success',
            'data'=>  new NoteOneResource($item),
        ]);
    }

    public function edit($id)
    {
        $item = Note::find($id);
        return response([
            'status' => 'success',
            'data'=>  new NoteOneResource($item),
        ]);
    }

    public function update(NoteRequest $request , $id)
    {
        $models = Note::findOrFail($id);

        $item = $this->accessLayer->update(
            $models,
            $request->input('note_title'),
            $request->input('description'),
            $request
        );
        return response([
            'status' => 'success',
            'data'=>  new NoteOneResource($item),
        ]);
    }

    public function destroy($id)
    {
        $model = Note::find($id);
        if ($model->delete())
        {
            return response([
                'status' => 'success',
                'message'=>  'have been delete',
        ]);
        }else{
            return response([
                'status' => 'failed',
                'message'=>  'error',
            ]);
        }
    }
}
