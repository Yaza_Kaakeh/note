<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Layers\BusinessLayer\API\Auth\AuthLayer;
use App\Models\User;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{

    public function __construct(AuthLayer $accesslayer)
    {
        $this->accesslayer = $accesslayer;
    }

    use ApiResponses;

    public function login(Request $request)
    {
        $attr = $this->accesslayer->LoginValidation($request);

        if (!$user = Auth::attempt($attr)) {
            return response([
                'status' => 'failed',
                'message' => 'Credentials not match',
                'code' => '401',
            ]);
        }
        if ($user) {
            $token = auth()->user()->createToken('API Token')->plainTextToken;
            return $this->successResponse('success', [
                'token' => $token,
            ]);
        } else {
            return response([
                'status' => 'failed',
            ]);
        }
    }

    public function createAccount(Request $request)
    {
        $attr = $this->accesslayer->LoginValidation($request);
        $user = $this->accesslayer->storeData($attr);
        if ($user) {
            $token = $user->createToken('tokens')->plainTextToken;
            return $this->successResponse('success', [
                'token' => $token,
            ]);
        } else {
            return response([
                'status' => 'failed',
            ]);
        }
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        return [
            'message' => 'Tokens Revoked',
        ];
    }
}
