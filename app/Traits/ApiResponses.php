<?php


namespace App\Traits;


trait ApiResponses
{

    protected function successResponse($msg, $data)
    {
        return response()->json([
            'message' => $msg,
            'data' => $data,
        ], 200);
    }
}