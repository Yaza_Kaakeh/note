<?php
/**
 * Created by PhpStorm.
 * User: STC
 * Date: 10/11/2021
 * Time: 06:27
 */

namespace App\Layers\BusinessLayer\notes;

use Illuminate\Support\Facades\File;
use App\Models\Note;
use Illuminate\Http\Request;

use Illuminate\Support\Str;

class NoteLayer
{

    public function update(Note $models,  $note_title, $description,Request $request)
    {

        $models->note_title = $note_title;
        $models->slug = Str::slug($note_title);
        $models->description = $description;


        if ($request->hasFile('image')){

            $path_exist = 'images/notes/'.$models->image;
            if (File::exists($path_exist))
            {
                File::delete($path_exist);
            }

            $path = 'images/notes';
            $photo_name = $this->insertImage($request->image ,$path ,$models->id);
            $models->image = $photo_name;
        }


        $models->save();
        return $models;
    }
    public function store($note_title, $description, Request $request)
    {
        $models = new Note();
        $models->note_title = $note_title;
        $models->slug = Str::slug($note_title);
        $models->description = $description;

        if ($request->hasFile('image')) {

            $path = 'images/notes';
            $photo_name = $this->insertImage($request->image, $path, $models->id);
            $models->image = $photo_name;
        }


        $models->save();
        return $models;
    }

    public function insertImage($photo, $path, $id)
    {
        $image = $photo;
        if ($image) {
            $ext = $image->getClientOriginalExtension();
            $name = $id . time() . '.' . $ext;
            //store in the public disk
            $image->move($path, $name);
        }
        return $name;
    }


    public function getValidationRules()
    {
        return [
            'note_title' => 'required|string|unique:notes,note_title',
            'photo' => 'nullable|file|mimetypes:image/png,image/jpg,image/jpeg,image/pjpeg|max:10240',
            'description' => 'nullable|string',

        ];
    }

    public function getValidationRulesUpdate()
    {
        return [
            'note_title' => 'required|string|unique:notes,note_title',
            'photo' => 'nullable|file|mimetypes:image/png,image/jpg,image/jpeg,image/pjpeg|max:10240',
            'description' => 'nullable|string',

        ];
    }

    /*    public function generateSlug($name, $id)
    {
        //if we are editing a product, we pass the productId
        $slug = Str::slug($name);

        if ($id) {
            $slugExist = Note::withTrashed()->where('slug', $slug)->where('id', '!=', $id)->count();
        } else {
            $slugExist = Note::withTrashed()->where('slug', $slug)->count();
        }

        if ($slugExist) {

            $counter = 1;

            while ($slugExist) {
                $newSlug = $slug . '-' . $counter++;
                if ($id) {
                    $slugExist = Note::withTrashed()->where('slug', $newSlug)->where('id', '!=', $id)->count();
                } else {
                    $slugExist = Note::withTrashed()->where('slug', $newSlug)->count();
                }
            }

            $slug = $newSlug;
        }

        return $slug;
    }*/

}