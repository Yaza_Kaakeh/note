<?php
/**
 * Created by PhpStorm.
 * User: STC
 * Date: 11/11/2021
 * Time: 02:35
 */

namespace App\Layers\BusinessLayer\API\Auth;


use App\Models\User;
use Illuminate\Http\Request;

class AuthLayer
{

    public function registerValidation(Request $request){
        $attr = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:6|confirmed'
        ]);
        return $attr;
    }
    public function LoginValidation(Request $request){
        $attr = $request->validate([
            'email' => 'required|string|email|',
            'password' => 'required|string|min:6'
        ]);
        return $attr;
    }
    public function storeData($attr){
        $user = User::create([
            'name' => $attr['name'],
            'password' => bcrypt($attr['password']),
            'email' => $attr['email']
        ]);
        return $user;

    }
}