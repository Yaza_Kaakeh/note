<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendSubscriptionExpireEmaillJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $expiredCustomers;
    public function __construct($expiredCustomers)
    {
        $this->expiredCustomers = $expiredCustomers;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {        info('Im in the Job Class');

        foreach ($this->expiredCustomers as $customer){
            $expireDate = Carbon::createFromFormat('Y-m-d',$customer-> subscription_end)->toDateString();
            $expireDate = null;
        }
    }
}
