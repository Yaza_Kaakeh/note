<?php

namespace App\Console\Commands;

use App\Jobs\SendSubscriptionExpireEmaillJob;
use App\Models\Customer;
use Illuminate\Console\Command;

class SubscriptionExpiryNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:SubscriptionExpireNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check whiCh Subscribe User has been End.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $expiredCustomers = Customer::where('subscription_end','<',now())->get();
        info('Im in the Command');
        dispatch(new SendSubscriptionExpireEmaillJob($expiredCustomers));
        return true;
    }
}
