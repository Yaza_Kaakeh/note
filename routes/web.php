<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NoteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('frontend');

Auth::routes();
Route::get('/show/{slug}', [App\Http\Controllers\HomeController::class, 'show'])->name('show');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::prefix('dashboard')->middleware(['auth', 'web'])->group(function () {
    Route::resource('notes', NoteController::class)->except(['show','destroy']);
    Route::get('notes/{slug}', '\App\Http\Controllers\NoteController@show')->name('notes.show');
    Route::get('notes/delete/{id}', '\App\Http\Controllers\NoteController@destroy')->name('note.destroy');
});
