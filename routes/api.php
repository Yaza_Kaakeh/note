<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Auth\AuthController;
use \App\Http\Controllers\OrderController;
use \App\Http\Controllers\API\Note\NoteApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('/login', [AuthController::class, 'login']);
Route::post('/logout', [AuthController::class, 'logout']);
Route::post('/register', [AuthController::class, 'createAccount']);
Route::get('/note/{slug}', [NoteApiController::class, 'show']);

Route::apiResource('order', OrderController::class);



Route::middleware(['auth:sanctum', 'api'])->group(function () {
    Route::get('/notes', [NoteApiController::class, 'index']);
    Route::get('/edit/{id}', [NoteApiController::class, 'edit']);
    Route::post('/store-note', [NoteApiController::class, 'store']);
    Route::put('/update-note/{id}', [NoteApiController::class, 'update']);
    Route::delete('/delete-note/{id}', [NoteApiController::class, 'destroy']);

});
