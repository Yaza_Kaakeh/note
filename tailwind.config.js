module.exports = {
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      // add font
      fontFamily:{
        body: ['Overpass','sans-serif'],
        hero: ['Langar','sans-serif']
      },
      // add custom colors
      colors:{
      primary: '#303669'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}